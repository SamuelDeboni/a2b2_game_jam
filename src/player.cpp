function void
update_player(Player *p)
{
	Vec2 mov = {
		(f32)IsKeyDown(KEY_D) - (f32)IsKeyDown(KEY_A),
		(f32)IsKeyDown(KEY_S) - (f32)IsKeyDown(KEY_W),
	};

	mov = normalize(mov);

	auto new_pos = p->position + mov * GetFrameTime() * 100;

	b32 can_move = true;

	Vec2 offsets[8] = {
		{ 8.0f,  8.0f},
		{ 8.0f, -8.0f},
		{-8.0f,  8.0f},
		{-8.0f, -8.0f},
		{ 0.0f,  8.0f},
		{ 0.0f, -8.0f},
		{ 8.0f,  0.0f},
		{-8.0f, -0.0f}
	};

	for (int i = 0; i < 8; i++) {
		Vec2i tile_pos = vec2i(new_pos + offsets[i]) / 16;
		tile_pos.x = CLAMP(tile_pos.x, 0, WORLD_SIZE - 1);
		tile_pos.y = CLAMP(tile_pos.y, 0, WORLD_SIZE - 1);

		auto tile = gw->tilemap[tile_pos.x + tile_pos.y * WORLD_SIZE];
		if ((tile & 0x40) && get_teid(tile_pos) != 0) {
			can_move = false;
			break;
		}
	}

	if (can_move) {
		p->position = new_pos; 
	}
}