function void
set_animation(Sprite *sprite, Animation animation)
{
	sprite->animation = animation;	
	sprite->timer = 0;
	sprite->frame = 0;
}

function void
draw_sprite(Sprite *sprite, Vec2 pos)
{
	// Do animation
	{
		f32 delta = GetFrameTime();
		if (delta > 0.1) delta = 0.1;
		sprite->timer += delta * sprite->speed;	

		// 10 FPS
		if (sprite->timer >= 0.1) {
			sprite->timer -= 0.1;

			sprite->frame = (sprite->frame + 1) % sprite->animation.count;
		}
	}


	Texture texture = get_texture(sprite->texture);

	Rectangle source = {};

	i32 ix = sprite->animation.start + sprite->frame;
	i32 iy = sprite->animation.row;

	source.x = ix * sprite->frame_size.x;
	source.y = iy * sprite->frame_size.y;
	source.width  = sprite->frame_size.x;
	source.height = sprite->frame_size.y;

	Rectangle dest = {
		pos.x - sprite->origin.x,
		pos.y - sprite->origin.y,
		(f32)sprite->frame_size.x,
		(f32)sprite->frame_size.y
	};

	DrawTextureTiled(texture, source, dest, {0, 0}, 0, 1, WHITE);
}
