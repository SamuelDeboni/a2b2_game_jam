function Mat4
mul(Mat4 b, Mat4 a)
{
    Mat4 r = {};
    
    r.x.x = a.x.x * b.x.x + a.x.y * b.y.x + a.x.z * b.z.x + a.x.w * b.w.x;
    r.x.y = a.x.x * b.x.y + a.x.y * b.y.y + a.x.z * b.z.y + a.x.w * b.w.y;
    r.x.z = a.x.x * b.x.z + a.x.y * b.y.z + a.x.z * b.z.z + a.x.w * b.w.z;
    r.x.w = a.x.x * b.x.w + a.x.y * b.y.w + a.x.z * b.z.w + a.x.w * b.w.w;
    
    r.y.x = a.y.x * b.x.x + a.y.y * b.y.x + a.y.z * b.z.x + a.y.w * b.w.x;
    r.y.y = a.y.x * b.x.y + a.y.y * b.y.y + a.y.z * b.z.y + a.y.w * b.w.y;
    r.y.z = a.y.x * b.x.z + a.y.y * b.y.z + a.y.z * b.z.z + a.y.w * b.w.z;
    r.y.w = a.y.x * b.x.w + a.y.y * b.y.w + a.y.z * b.z.w + a.y.w * b.w.w;
    
    r.z.x = a.z.x * b.x.x + a.z.y * b.y.x + a.z.z * b.z.x + a.z.w * b.w.x;
    r.z.y = a.z.x * b.x.y + a.z.y * b.y.y + a.z.z * b.z.y + a.z.w * b.w.y;
    r.z.z = a.z.x * b.x.z + a.z.y * b.y.z + a.z.z * b.z.z + a.z.w * b.w.z;
    r.z.w = a.z.x * b.x.w + a.z.y * b.y.w + a.z.z * b.z.w + a.z.w * b.w.w;
    
    r.w.x = a.w.x * b.x.x + a.w.y * b.y.x + a.w.z * b.z.x + a.w.w * b.w.x;
    r.w.y = a.w.x * b.x.y + a.w.y * b.y.y + a.w.z * b.z.y + a.w.w * b.w.y;
    r.w.z = a.w.x * b.x.z + a.w.y * b.y.z + a.w.z * b.z.z + a.w.w * b.w.z;
    r.w.w = a.w.x * b.x.w + a.w.y * b.y.w + a.w.z * b.z.w + a.w.w * b.w.w;
    
    return r;
}

function Mat4
transposed(Mat4 a)
{
    Mat4 r = {
        a.x.x, a.y.x, a.z.x, a.w.x,
        a.x.y, a.y.y, a.z.y, a.w.y,
        a.x.z, a.y.z, a.z.z, a.w.z,
        a.x.w, a.y.w, a.z.w, a.w.w,
    };
    return r;
}

function Mat4
perspective_mat(f32 near, f32 far, f32 fov, f32 h2w)
{
    f32 _fov = fov * 3.1415926535 / 90.0;
    f32 S = cos_f32(_fov) / sin_f32(_fov);
    f32 s_h2w = S * h2w;
    f32 f1 = -(far / (far - near));
    f32 f2 = near * f1;
    
    Mat4 r = {
        -s_h2w, 0, 0, 0,
        0, -S, 0, 0,
        0, 0, f1, f2,
        0, 0, -1, 0,
    };
    
    return r;
}


function f32
lerp(f32 a, f32 b, f32 t) {
    return (1 - t) * a + t * b;
}

function Vec2
lerp(Vec2 a, Vec2 b, f32 t) {
    return Vec2{lerp(a.x, b.x, t), lerp(a.y, b.y, t)};
}

function Vec3
lerp(Vec3 a, Vec3 b, f32 t) {
    return Vec3{lerp(a.x, b.x, t),lerp(a.y, b.y, t),lerp(a.z, b.z, t)};
}

function Vec4
lerp(Vec4 a, Vec4 b, f32 t) {
    return Vec4{
        lerp(a.x, b.x, t),lerp(a.y, b.y, t),
        lerp(a.z, b.z, t),lerp(a.w, b.w, t)
    };
}

function f32
smoothstep(f32 a, f32 b, f32 w)
{
    if (0.0 > w) return a;
    if (1.0 < w) return b;
    
    //return (a1 - a0) * w + a0;
    return (b - a) * (3.0 - w * 2.0) * w * w + a;
}


function f32 floor_f32(f32 f) {
    int i = (int)f;
    return (float)i;
}

function f32 ceil_f32(f32 f) {
    int i = (int)f;
    return (float)(i + 1);
}

function f32 dot(Vec2 a, Vec2 b) {
    f32 f = a.x * b.x + a.y * b.y;
    return f;
}

function f32 len2(Vec2 a) {
    f32 l = dot(a, a);
    return l;
}

function f32 len(Vec2 a) {
    return sqrt_f32(len2(a));
}

function f32 dot(Vec3 a, Vec3 b) {
    f32 f = a.x * b.x + a.y * b.y + a.z * b.z;
    return f;
}

function f32 len2(Vec3 a) {
    f32 l = dot(a, a);
    return l;
}

function f32 len(Vec3 a) {
    return sqrt_f32(len2(a));
}

function f32 magnitude(Vec2 v) {
    return sqrt_f32(dot(v, v));
}

function Vec2 normalize(Vec2 v) {
    f32 len = sqrt_f32(dot(v, v));
    if (len == 0.0) return {};
    return v / len;
}

function Vec3 normalize(Vec3 v) {
    f32 len = sqrt_f32(dot(v, v));
    if (len == 0.0) return {};
    return v / len;
}

function Vec2
random_gradient(Vec2i i)
{
    auto v = vec2(i);
    
    auto random = 2920.0 * sin_f32(v.x * 21942.0 + v.y * 171324.0 + 8912.0) *
        cos_f32(v.x * 23157.0 * v.y * 217832.0 + 9758.0);
    return vec2(cos_f32(random), sin_f32(random));
}

function f32
dot_grid_gradient(Vec2i i, Vec2 f)
{
    Vec2 grad = random_gradient(i);
    Vec2 d = f - vec2(i);
    return dot(grad, d);
}

function f32
perlin(Vec2 v)
{
    Vec2i v0 = vec2i(v);
    Vec2i v1 = v0 + 1;
    
    Vec2 s = v - vec2(v0);
    
    f32 n0  = dot_grid_gradient(v0, v);
    f32 n1  = dot_grid_gradient(vec2i(v1.x, v0.y), v);
    f32 ix0 = smoothstep(n0, n1, s.x);
    
    n0 = dot_grid_gradient(vec2i(v0.x, v1.y), v);
    n1 = dot_grid_gradient(v1, v);
    f32 ix1 = smoothstep(n0, n1, s.x);
    
    f32 value = smoothstep(ix0, ix1, s.y);
    
    return value;
}



static uint32_t
crc32_for_byte(uint32_t r)
{
    for(int j = 0; j < 8; ++j) {
        r = (r & 1? 0: (uint32_t)0xEDB88320L) ^ r >> 1;
    }
    return r ^ (uint32_t)0xFF000000L;
}

static void
crc32(const void *data, size_t n_bytes, uint32_t* crc)
{
    static uint32_t table[0x100];
    if(!*table) {
        for(size_t i = 0; i < 0x100; ++i) {
            table[i] = crc32_for_byte(i);
        }
    }
    
    for(size_t i = 0; i < n_bytes; ++i) {
        *crc = table[(uint8_t)*crc ^ ((uint8_t*)data)[i]] ^ *crc >> 8;
    }
}

static uint32_t
crc32_string(const char *str)
{
    uint32_t crc = 0;

    static uint32_t table[0x100];
    if(!*table) {
        for(size_t i = 0; i < 0x100; ++i) {
            table[i] = crc32_for_byte(i);
        }
    }
    
    for(size_t i = 0; str[i] != 0; ++i) {
        crc = table[(uint8_t)crc ^ ((const uint8_t*)str)[i]] ^ crc >> 8;
    }

    return crc;
}