#ifndef ARENAS_H
#define ARENAS_H

typedef void* Mem_ReserveFunction(void *ctx, u64 size);
typedef void  Mem_ChangeMemoryFunction(void *ctx, void *ptr, u64 size);

struct BaseMemory {
	Mem_ReserveFunction      *reserve;
	Mem_ChangeMemoryFunction *commit;
	Mem_ChangeMemoryFunction *decommit;
	Mem_ChangeMemoryFunction *release;
	void *ctx;
};

struct Arena {
	BaseMemory base_memory;
	void *mem;
	u64 capacity;

	u64 offset;
	u64 prev_offset;
};

#define DEFAULT_ALIGN 16
#define MEMSET(m, v, l) for(u64 __i = 0; __i < (l); __i++) {(m)[__i] = (v);}

#define PUSH_ARRAY(a, T, c) (T*)arena_push((a), sizeof(T)*(c))

function BaseMemory malloc_base_memory();
function Arena create_arena(BaseMemory base_memory, u64 capacity);
function Arena create_arena(BaseMemory base_memory, void *mem, u64 capacity);
function void arena_release(Arena *arena);
function void *arena_push(Arena *arena, u64 size, u64 align = DEFAULT_ALIGN);
function void arena_pop(Arena *arena);
function void arena_pop_to(Arena *arena, u64 offset, u64 align = DEFAULT_ALIGN);
function void arena_pop_count(Arena *arena, u64 count, u64 align = DEFAULT_ALIGN);

// TODO(samuel): add the ability to only commit when needed

#endif // ARENAS
