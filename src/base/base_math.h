/* date = August 25th 2021 7:14 pm */
// NOTE(samuel): This is a c++ header

#ifndef DMATH_H
#define DMATH_H

#include <math.h>

//~
union Vec2i {
    struct { i32 x, y; };
    i32 arr[2];
};

union Vec3i {
    struct { i32 x, y, z; };
    struct { i32 r, g, b; };
    i32 arr[3];
};

union Vec2 {
    struct { f32 x, y; };
    f32 arr[2];
};

union Vec3 {
    struct { f32 x, y, z; };
    struct { f32 r, g, b; };
    struct { f32 _x; Vec2 yz; };
    Vec2 xy;
    f32 arr[3];
};

union Vec4 {
    struct { f32 x, y, z, w; };
    struct { f32 r, g, b, a; };
    struct { Vec2 xy, zw; };
    Vec3 xyz;
    Vec3 rgb;
    f32 arr[4];
};

union Mat4 {
    struct { Vec4 x, y, z, w; };
    f32 m[4][4];
    f32 arr[16];
};

//~ NOTE(samuel): Constructors

//- Vec2i
inline Vec2i vec2i(i32 x, i32 y) {
    return Vec2i{x, y};
}

inline Vec2i vec2i(Vec2 vf) {
    return Vec2i{(i32)vf.x, (i32)vf.y};
}

//- Vec2
inline Vec2 vec2(f32 x, f32 y) {
    return Vec2{x, y};
}

inline Vec2 vec2(Vec2i vi) {
    return Vec2{(f32)vi.x, (f32)vi.y};
}

//- Vec3i
inline Vec3i vec3i(i32 x, i32 y, i32 z) {
    return Vec3i{x, y, z};
}

inline Vec3i vec3i(Vec3i vi) {
    return Vec3i{(i32)vi.x, (i32)vi.y, (i32)vi.z};
}

//- Vec3
inline Vec3 vec3(f32 x, f32 y, f32 z) {
    return Vec3{x, y, z};
}

inline Vec3 vec3(Vec3i vi) {
    return Vec3{(f32)vi.x, (f32)vi.y, (f32)vi.z};
}

//~ NOTE(samuel): Vec2 Vec2 operations
inline Vec2 operator+(Vec2 a, Vec2 b) { return Vec2{a.x + b.x, a.y + b.y}; }
inline Vec2 operator-(Vec2 a, Vec2 b) { return Vec2{a.x - b.x, a.y - b.y}; }
inline Vec2 operator*(Vec2 a, Vec2 b) { return Vec2{a.x * b.x, a.y * b.y}; }
inline Vec2 operator/(Vec2 a, Vec2 b) { return Vec2{a.x / b.x, a.y / b.y}; }

inline void operator+=(Vec2 &a, Vec2 b) { a = a + b; }
inline void operator-=(Vec2 &a, Vec2 b) { a = a - b; }
inline void operator*=(Vec2 &a, Vec2 b) { a = a * b; }
inline void operator/=(Vec2 &a, Vec2 b) { a = a / b; }

//~ NOTE(samuel): Vec2 f32 operations
inline Vec2 operator+(Vec2 a, f32 f) { return Vec2{a.x + f, a.y + f}; }
inline Vec2 operator-(Vec2 a, f32 f) { return Vec2{a.x - f, a.y - f}; }
inline Vec2 operator*(Vec2 a, f32 f) { return Vec2{a.x * f, a.y * f}; }
inline Vec2 operator/(Vec2 a, f32 f) { return Vec2{a.x / f, a.y / f}; }

//~ NOTE(samuel): f32 Vec2 operations
inline Vec2 operator+(f32 f, Vec2 a) { return Vec2{f + a.x, f + a.y}; }
inline Vec2 operator-(f32 f, Vec2 a) { return Vec2{f - a.x, f - a.y}; }
inline Vec2 operator*(f32 f, Vec2 a) { return Vec2{f * a.x, f * a.y}; }
inline Vec2 operator/(f32 f, Vec2 a) { return Vec2{f / a.x, f / a.y}; }

//~ NOTE(samuel): Vec2i Vec2i operations
inline Vec2i operator+(Vec2i a, Vec2i b) { return Vec2i{a.x + b.x, a.y + b.y}; }
inline Vec2i operator-(Vec2i a, Vec2i b) { return Vec2i{a.x - b.x, a.y - b.y}; }
inline Vec2i operator*(Vec2i a, Vec2i b) { return Vec2i{a.x * b.x, a.y * b.y}; }
inline Vec2i operator/(Vec2i a, Vec2i b) { return Vec2i{a.x / b.x, a.y / b.y}; }

inline void operator+=(Vec2i &a, Vec2i b) { a = a + b; }
inline void operator-=(Vec2i &a, Vec2i b) { a = a - b; }
inline void operator*=(Vec2i &a, Vec2i b) { a = a * b; }
inline void operator/=(Vec2i &a, Vec2i b) { a = a / b; }

//~ NOTE(samuel): Vec2i f32 operations
inline Vec2i operator+(Vec2i a, i32 f) { return Vec2i{a.x + f, a.y + f}; }
inline Vec2i operator-(Vec2i a, i32 f) { return Vec2i{a.x - f, a.y - f}; }
inline Vec2i operator*(Vec2i a, i32 f) { return Vec2i{a.x * f, a.y * f}; }
inline Vec2i operator/(Vec2i a, i32 f) { return Vec2i{a.x / f, a.y / f}; }

//~ NOTE(samuel): i32 Vec2i operations
inline Vec2i operator+(i32 f, Vec2i a) { return Vec2i{f + a.x, f + a.y}; }
inline Vec2i operator-(i32 f, Vec2i a) { return Vec2i{f - a.x, f - a.y}; }
inline Vec2i operator*(i32 f, Vec2i a) { return Vec2i{f * a.x, f * a.y}; }
inline Vec2i operator/(i32 f, Vec2i a) { return Vec2i{f / a.x, f / a.y}; }


//~ NOTE(samuel): Vec3 Vec3 operations
inline Vec3 operator+(Vec3 a, Vec3 b) { return Vec3{a.x + b.x, a.y + b.y, a.z + b.z}; }
inline Vec3 operator-(Vec3 a, Vec3 b) { return Vec3{a.x - b.x, a.y - b.y, a.z - b.z}; }
inline Vec3 operator*(Vec3 a, Vec3 b) { return Vec3{a.x * b.x, a.y * b.y, a.z * b.z}; }
inline Vec3 operator/(Vec3 a, Vec3 b) { return Vec3{a.x / b.x, a.y / b.y, a.z / b.z}; }

inline void operator+=(Vec3 &a, Vec3 b) { a = a + b; }
inline void operator-=(Vec3 &a, Vec3 b) { a = a - b; }
inline void operator*=(Vec3 &a, Vec3 b) { a = a * b; }
inline void operator/=(Vec3 &a, Vec3 b) { a = a / b; }

//~ NOTE(samuel): Vec3 f32 operations
inline Vec3 operator+(Vec3 a, f32 f) { return Vec3{a.x + f, a.y + f, a.z + f}; }
inline Vec3 operator-(Vec3 a, f32 f) { return Vec3{a.x - f, a.y - f, a.z - f}; }
inline Vec3 operator*(Vec3 a, f32 f) { return Vec3{a.x * f, a.y * f, a.z * f}; }
inline Vec3 operator/(Vec3 a, f32 f) { return Vec3{a.x / f, a.y / f, a.z / f}; }

//~ NOTE(samuel): f32 Vec3 operations
inline Vec3 operator+(f32 f, Vec3 a) { return Vec3{f + a.x, f + a.y, f + a.z}; }
inline Vec3 operator-(f32 f, Vec3 a) { return Vec3{f - a.x, f - a.y, f - a.z}; }
inline Vec3 operator*(f32 f, Vec3 a) { return Vec3{f * a.x, f * a.y, f * a.z}; }
inline Vec3 operator/(f32 f, Vec3 a) { return Vec3{f / a.x, f / a.y, f / a.z}; }


//~ NOTE(samuel): Vec3i Vec3i operations
inline Vec3i operator+(Vec3i a, Vec3i b) { return Vec3i{a.x + b.x, a.y + b.y, a.z + b.z}; }
inline Vec3i operator-(Vec3i a, Vec3i b) { return Vec3i{a.x - b.x, a.y - b.y, a.z - b.z}; }
inline Vec3i operator*(Vec3i a, Vec3i b) { return Vec3i{a.x * b.x, a.y * b.y, a.z * b.z}; }
inline Vec3i operator/(Vec3i a, Vec3i b) { return Vec3i{a.x / b.x, a.y / b.y, a.z / b.z}; }
inline b32 operator==(Vec3i a, Vec3i b) { return (a.x == b.x && a.y == b.y && a.z == b.z);}
inline b32 operator!=(Vec3i a, Vec3i b) { return (a.x != b.x && a.y != b.y && a.z != b.z);}

inline void operator+=(Vec3i &a, Vec3i b) { a = a + b; }
inline void operator-=(Vec3i &a, Vec3i b) { a = a - b; }
inline void operator*=(Vec3i &a, Vec3i b) { a = a * b; }
inline void operator/=(Vec3i &a, Vec3i b) { a = a / b; }

//~ NOTE(samuel): Vec3i i32 operations
inline Vec3i operator+(Vec3i a, i32 f) { return Vec3i{a.x + f, a.y + f, a.z + f}; }
inline Vec3i operator-(Vec3i a, i32 f) { return Vec3i{a.x - f, a.y - f, a.z - f}; }
inline Vec3i operator*(Vec3i a, i32 f) { return Vec3i{a.x * f, a.y * f, a.z * f}; }
inline Vec3i operator/(Vec3i a, i32 f) { return Vec3i{a.x / f, a.y / f, a.z / f}; }

//~ NOTE(samuel): i32 Vec3i operations
inline Vec3i operator+(i32 f, Vec3i a) { return Vec3i{f + a.x, f + a.y, f + a.z}; }
inline Vec3i operator-(i32 f, Vec3i a) { return Vec3i{f - a.x, f - a.y, f - a.z}; }
inline Vec3i operator*(i32 f, Vec3i a) { return Vec3i{f * a.x, f * a.y, f * a.z}; }
inline Vec3i operator/(i32 f, Vec3i a) { return Vec3i{f / a.x, f / a.y, f / a.z}; }


//~ NOTE(samuel): Vec4 Vec4 operations
inline Vec4 operator+(Vec4 a, Vec4 b) { return Vec4{a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w}; }
inline Vec4 operator-(Vec4 a, Vec4 b) { return Vec4{a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w}; }
inline Vec4 operator*(Vec4 a, Vec4 b) { return Vec4{a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w}; }
inline Vec4 operator/(Vec4 a, Vec4 b) { return Vec4{a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w}; }

//~ NOTE(samuel): Vec4 f32 operations
inline Vec4 operator+(Vec4 a, f32 f) { return Vec4{a.x + f, a.y + f, a.z + f, a.w + f}; }
inline Vec4 operator-(Vec4 a, f32 f) { return Vec4{a.x - f, a.y - f, a.z - f, a.w - f}; }
inline Vec4 operator*(Vec4 a, f32 f) { return Vec4{a.x * f, a.y * f, a.z * f, a.w * f}; }
inline Vec4 operator/(Vec4 a, f32 f) { return Vec4{a.x / f, a.y / f, a.z / f, a.w / f}; }

//~ NOTE(samuel): f32 Vec4 operations
inline Vec4 operator+(f32 f, Vec4 a) { return Vec4{f + a.x, f + a.y, f + a.z, f + a.w}; }
inline Vec4 operator-(f32 f, Vec4 a) { return Vec4{f - a.x, f - a.y, f - a.z, f - a.w}; }
inline Vec4 operator*(f32 f, Vec4 a) { return Vec4{f * a.x, f * a.y, f * a.z, f * a.w}; }
inline Vec4 operator/(f32 f, Vec4 a) { return Vec4{f / a.x, f / a.y, f / a.z, f / a.w}; }

//~NOTE(samuel): Mat4 Functions

function Mat4 mul(Mat4 b, Mat4 a);

inline Mat4 operator*(Mat4 b, Mat4 a) {return mul(b, a);}
inline void operator*=(Mat4 &b, Mat4 a) {b = mul(b, a);}

function Mat4 transposed(Mat4 a);
function Mat4 perspective_mat(f32 near, f32 far, f32 fov, f32 h2w);

// General math functions

function f32 sin_f32(f32 f) {return sinf(f);}
function f32 cos_f32(f32 f) {return cosf(f);}
function f32 tan_f32(f32 f) {return tanf(f);}
function f32 sqrt_f32(f32 f) {return sqrtf(f);}

function f32 lerp(f32 a, f32 b, f32 t);
function Vec2 lerp(Vec2 a, Vec2 b, f32 t);
function Vec3 lerp(Vec3 a, Vec3 b, f32 t);
function Vec4 lerp(Vec4 a, Vec4 b, f32 t);
function f32 smoothstep(f32 a, f32 b, f32 w);
function f32 floor_f32(f32 f);
function f32 ceil_f32(f32 f);
function f32 dot(Vec2 a, Vec2 b);
function f32 len2(Vec2 a);
function f32 len(Vec2 a);
function f32 dot(Vec3 a, Vec3 b);
function f32 len2(Vec3 a);
function f32 len(Vec3 a);
function f32 magnitude(Vec2 v);
function Vec2 normalize(Vec2 v);
function Vec3 normalize(Vec3 v);

// Perlin noise 
function Vec2 random_gradient(Vec2i i);
function f32  dot_grid_gradient(Vec2i i, Vec2 f);
function f32  perlin(Vec2 v);

// Hash functions
static uint32_t crc32_for_byte(uint32_t r);
static void crc32(const void *data, size_t n_bytes, uint32_t* crc);
static uint32_t crc32_string(const char *str);

#endif //DMATH_H
