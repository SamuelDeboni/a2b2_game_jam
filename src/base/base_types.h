#ifndef BASE_TYPES_H
#define BASE_TYPES_H


// ===== Usefull Macros =====
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define CLAMP(v, a, b) MAX((a), MIN((v), (b)))

#define STMNT(s) do { s } while(0)

#if !defined(ASSERT_BREAK)
# define ASSERT_BREAK() (*(int *)0 = 0)
#endif

#if defined(ENABLE_ASSERT)
# define ASSERT(c) STMNT(if (!(c)) { ASSERT_BREAK(); })
#else
# define ASSERT(c)
#endif

// ===== Basic types =====
#include <stdint.h>

#define internal static

#define function   static
#define global     static
#define persistent static

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float  f32;
typedef double f64;

typedef uint8_t  b8;
typedef uint32_t b32;

typedef void VoidFunc(void);

// ===== Basic Constants =====

// TODO: Add basic constants
#if !defined(PI)
#define PI 3.141592653f
#endif

#define KILOBYTE(x) ((x) * (u64)1024L)
#define MEGABYTE(x) (KILOBYTE(x) * (u64)1024L)
#define GIGABYTE(x) (MEGABYTE(x) * (u64)1024L)

#endif