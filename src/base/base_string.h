/* date = October 29th 2020 3:19 pm */

#ifndef BASE_STRING_H
#define BASE_STRING_H

#define SFOR(s) for (uint32_t i = 0, it = s.ptr[0]; \
i < s.len; \
it = s.ptr[++i])

// ===== Definitions =====
struct String8
{
    char *ptr;
    u32 len;
};

struct Substring8
{
    u32 offset;
    u32 len;
};

function inline void
offset_string(String8 *s, i32 offset)
{
    s->ptr += offset;
    s->len -= offset;
}

// Constructors
#define str8_lit(s) (String8){(s), sizeof((s)) - 1};
function String8 str8(char *buffer, u32 len);

function void to_cstring(char *buffer, u64 capacity, String8 string);

function String8 next_token(String8 *s, char separator);
function String8 get_line(String8 *s);
function String8 literal_to_string(char *sl);
function String8 string_between(String8 s, String8 first, String8 second);

function Substring8 substring(String8 s, String8 sub);
function b32 equal(String8 s1, String8 s2);

function void remove_trailing_spaces(String8 *s);
function void remove_leading_spaces(String8 *s);
function void to_cstring(char *buffer, u64 capacity, String8 string);



#endif //DEBONI_STRING_H