function String8
str8(char *buffer, u32 len)
{
    String8 result = {buffer, len};
    return result;
}

function void
to_cstring(char *buffer, uint64_t capacity, String8 string)
{
    int i = 0;
    for (; i < string.len && i < capacity - 1; i++) {
        buffer[i] = string.ptr[i];
    }
    
    buffer[i] = 0;
}

function String8
next_token(String8 *s, char separator)
{
    String8 result = *s;
    
    result.len = 0;
    u32 max_len = s->len;
    while ((result.len < max_len) && (*(s->ptr) != separator)) {
        result.len++;
        s->len--;
        s->ptr++;
    }
    
    if (s->len != 0) {
        s->ptr++;
        s->len--;
    }
    
    return result;
}

// NOTE(Samuel): Works with both CRLF and LF
function String8
get_line(String8 *s)
{
    String8 line = next_token(s, '\n');
    if (line.ptr[line.len-1] == '\r') line.len--;
    return line;
}

// NOTE(Samuel): Return a String8 using the memory of a string literal
// You cannot chant its content, i think
function String8
literal_to_string(char *sl)
{
    String8 result = {0};
    result.ptr = (char *)sl;
    while (*(sl++) != 0) {
        result.len++;
    }
    return result;
}

function Substring8
substring(String8 s, String8 sub)
{
    Substring8 result = {0};
    
    u32 i;
    for (i = 0; i < (s.len - sub.len + 1); i++) {
        if (s.ptr[i] == sub.ptr[0]) {
            b32 found = true;
            for (u32 j = 0; j < sub.len; j++) {
                if (s.ptr[i + j] != sub.ptr[j]) {
                    found = false;
                    break;
                }
            }
            if (found) {
                result.offset = i;
                result.len = sub.len;
                break;
            } 
        }
    }
    
    return result;
}

function String8
string_between(String8 s, String8 first, String8 second)
{
    String8 result = s;
    
    Substring8 sub = substring(s, first);
    offset_string(&result, (i32)(sub.offset + sub.len));
    sub = substring(result, second);
    
    result.len = sub.offset;
    
    return result;
}

function void
remove_trailing_spaces(String8 *s)
{
    while (s->ptr[s->len - 1] == ' ' && s->len > 0) {
        s->len--;
    }
}

function void
remove_leading_spaces(String8 *s)
{
    while (s->ptr[0] == ' ' && s->len > 0) {
        offset_string(s, 1);
    }
}

function b32
equal(String8 s1, String8 s2)
{
    if (s1.len != s2.len) {
        return false;
    }
    
    if (s1.ptr == s2.ptr) {
        return true;
    }
    
    SFOR(s1) {
        if (it != s2.ptr[i]) {
            return false;
        } 
    }
    
    return true;
}
