#include "base_mem.h"

#include <stdlib.h>

function void *
mem_malloc_reserve(void *ctx, u64 size)
{
    return malloc(size);
}

function void
mem_malloc_commit(void *ctx, void *ptr, u64 size)
{

}

function void
mem_malloc_decommit(void *ctx, void *ptr, u64 size)
{
    
}

function void
mem_malloc_release(void *ctx, void *ptr, u64 size)
{
    free(ptr);
}

function BaseMemory
malloc_base_memory()
{
    persistent BaseMemory memory = {};
    if (memory.reserve == 0) {
        memory.reserve = mem_malloc_reserve;
        memory.commit = mem_malloc_commit;
        memory.decommit = mem_malloc_decommit;
        memory.release = mem_malloc_release;
    }
    return memory;
}

function Arena
create_arena(BaseMemory base_memory, void *mem, u64 capacity)
{
    Arena arena = {};
    arena.base_memory = base_memory;
    arena.mem = mem;
    arena.capacity = capacity;
    return arena;
}

function Arena
create_arena(BaseMemory base_memory, u64 capacity)
{
	return create_arena(base_memory, base_memory.reserve(base_memory.ctx, capacity), capacity);
}

function void
arena_release(Arena *arena)
{
    ASSERT(arena);
    arena->base_memory.release(arena->base_memory.ctx, arena->mem, arena->capacity);
}

function u64
align_forward(u64 ptr, u64 align)
{
    u64 p = ptr;
    u64 a = (u64)align;
    u64 modulo = p & (a - 1);
    
    if (modulo != 0) {
        p += a - modulo;
    }
    
    return p;
}

function void *
arena_push(Arena *arena, u64 size, u64 align)
{
	void *result = 0;
    
    u64 offset = (u64)arena->mem + (u64)arena->offset; 
    offset = align_forward(offset, align);
    offset -= (u64)arena->mem;
    
    if (offset + size <= arena->capacity) {
        result = (u8 *)arena->mem + offset;
        arena->prev_offset = arena->offset;
        arena->offset = offset + size;
        MEMSET((u8 *)result, 0, size);

	    for (u64 i = 0; i < size; i++) ((u8 *)result)[i] = 0;
    }
    
    return result;
}

function void
arena_pop(Arena *arena)
{
    arena->offset = arena->prev_offset;
}

function void
arena_pop_to(Arena *arena, u64 offset, u64 align)
{
    arena->offset = align_forward(offset + (u64)arena->mem, align) - (u64)arena->mem;
}

function void
arena_pop_count(Arena *arena, u64 count, u64 align)
{
    arena_pop_to(arena, arena->offset - count, align);
}
