#ifndef ASSETS_H
#define ASSETS_H

enum AssetType {
	AssetType_Texture,
};

enum AssetId {
	Asset_Potato = 0,
	Asset_DebugTileset,
	Asset_Tileset,
	Asset_AutoBuilderSheet,
	Asset_AutoDrillSheet,
	Asset_AutoFurnaceSheet,
	Asset_TowerSheet,
	Asset_Count,
};

struct AssetEntry {
	AssetType type;
	const char *filename;

	Texture texture;
};


function void load_assets();
function Texture get_texture(AssetId id);

#endif // ASSETS_H