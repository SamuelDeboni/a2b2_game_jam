
global AssetEntry asset_table[Asset_Count] = {
	{AssetType_Texture, "assets/potato.png"},
	{AssetType_Texture, "assets/Tilemap_Debug.png"},
	{AssetType_Texture, "assets/Tileset.png"},

	{AssetType_Texture, "assets/AutoBuilder_Sheet.png"},
	{AssetType_Texture, "assets/AutoDrill_Sheet.png"},
	{AssetType_Texture, "assets/AutoFurnace_Sheet.png"},
	{AssetType_Texture, "assets/TransmissionTower_Sheet.png"},
};


function void
load_assets()
{
	for (int i = 0; i < Asset_Count; i++) {
		if (asset_table[i].type == AssetType_Texture) {
			asset_table[i].texture = LoadTexture(asset_table[i].filename);
		}
	}
}

function Texture
get_texture(AssetId id)
{
	if (asset_table[id].type == AssetType_Texture) {
		return asset_table[id].texture;
	}
	
	return asset_table[0].texture;
}
