function void
update_world()
{
	f32 delta = GetFrameTime();
	if (delta > 0.1) delta = 0.1;

	// Update tiles



	// Consume energy
	for (int i = 1; i < gw->te_list.count; i++) {
		TileEntity *te = &gw->te_list.data[i];
		if (te->id == 0) continue;

		if (te->type == TE_Drill) {
			f32 de = delta;
			if (te->energy_buffer > de) {
				te->energy_buffer -= de;
			}
		} else if (te->type == TE_Base) {
#if 0
			f32 de = delta * 10;
			if (te->energy_buffer + de < te->buffer_capacity) {
				te->energy_buffer += de;
			}
#endif
		}
	}

	// Transmit Energy
	for (int i = 1; i < gw->te_list.count; i++) {
		TileEntity *te = &gw->te_list.data[i];
		if (te->id == 0) continue;

		if (te->type == TE_Pole) {

			// Get from base			
			{
				TeId te2_id = 0;
				for (i32 y = -5; y <= 5; y++) {
					for (i32 x = -5; x <= 5; x++) {
						auto _id = get_teid(vec2i(x, y) + te->pos);
						if (te_ptr(_id)->type == TE_Base) {
							te2_id = _id;
							break;
						}

						if (te2_id) {
							break;
						}
					}
				}

				auto te2 = te_ptr(te2_id);
				f32 de = te->buffer_capacity - te->energy_buffer;
				de = CLAMP(de, 0, te2->energy_buffer);
				te2->energy_buffer -= de;
				te->energy_buffer += de;
			}

			// Get energy from other poles
			{
				Vec2i pos = te->pos;
				pos.y -= 1;
				auto te2_id = get_pole_conection(&pos, te->rotation);
				auto te2 = te_ptr(te2_id);
				f32 de = te->buffer_capacity - te->energy_buffer;
				de = CLAMP(de, 0, te2->energy_buffer);
				te2->energy_buffer -= de;
				te->energy_buffer += de;
			}
		} else if (te->type == TE_Drill) {
			TeId te2_id = 0;
			for (i32 y = -5; y <= 5; y++) {
				for (i32 x = -5; x <= 5; x++) {
					auto _id = get_teid(vec2i(x, y) + te->pos);
					if (te_ptr(_id)->type == TE_Pole) {
						te2_id = _id;
						break;
					}

					if (te2_id) {
						break;
					}
				}
			}

			auto te2 = te_ptr(te2_id);
			f32 de = te->buffer_capacity - te->energy_buffer;
			de = CLAMP(de, 0, te2->energy_buffer);
			te2->energy_buffer -= de;
			te->energy_buffer += de;

			if (te->energy_buffer <= 0.01) {
				te->sprite.speed = 0;
			} else {
				te->sprite.speed = 1;
			}
		}
	}
}

function TeId
get_pole_conection(Vec2i *pos, i32 rotation)
{
	TeId id = 0;

	for (int i = 0; i < 8; i++) {
		if (rotation == 0) {
			pos->y -= 1;
		} else if (rotation == 1) {
			pos->x -= 1;
		} else if (rotation == 2) {
			pos->y += 1;
			if (i == 0) continue;
		} else {
			pos->x += 1;
		}

		if (pos->x < 0 || pos->x >= WORLD_SIZE || pos->y < 0 || pos->y >= WORLD_SIZE) {
			break;
		}

		if (te_ptr(gw->te_ids[pos->y][pos->x])->type == TE_Pole &&
			gw->te_ids[pos->y][pos->x] == gw->te_ids[pos->y + 1][pos->x])
		{
			id = gw->te_ids[pos->y][pos->x];
			break;
		}
	}

	return id;
}

function void
draw_tile_entity(TileEntity *te)
{
	switch (te->type) {
		case TE_Null: {} break;

		case TE_Pole: {
			draw_sprite(&te->sprite, {te->pos.x * 16.0f, te->pos.y * 16.0f});

			Vec2i pos = te->pos;
			pos.y -= 1;
			auto te2_id = get_pole_conection(&pos, te->rotation);

			if (te2_id != 0) {
				DrawLineEx(
					{te->pos.x * 16.0f + 8, te->pos.y * 16.0f - 16.0f},
					{pos.x * 16.0f + 8, pos.y * 16.0f},
					4, BLUE);
			}

		} break;

		case TE_Drill: {
			draw_sprite(&te->sprite, {te->pos.x * 16.0f, te->pos.y * 16.0f});
		} break;

		case TE_Base: {
			draw_sprite(&te->sprite, {te->pos.x * 16.0f, te->pos.y * 16.0f});
		} break;
	}

	char buffer[256];
	sprintf(buffer, "%.1f", te->energy_buffer);
	DrawText(buffer, te->pos.x * 16, te->pos.y * 16, 4, RED);
}

function void
draw_world()
{
	// Draw Tileset
	for (int y = 0; y < WORLD_SIZE; y++) {
		for (int x = 0; x < WORLD_SIZE; x++) {
			Rectangle source = {};

			i32 tile = (i32)gw->tilemap[x + y * WORLD_SIZE];

			i32 ix = tile % 16;
			i32 iy = tile / 16;

			source.x = ix * 16;
			source.y = iy * 16;
			source.width = 16;
			source.height = 16;

			DrawTextureTiled(
				get_texture(Asset_Tileset),
				source,
				{(f32)x * 16.0f, (f32)y * 16.0f, 16.0f, 16.0f},
				{0, 0},
				0, 1, WHITE);				
		}
	}

	// Draw tile entities 
	for (int i = 1; i < gw->te_list.count; i++) {
		TileEntity *te = &gw->te_list.data[i];
		if (te->id == 0) continue;

		draw_tile_entity(te);
	}

	// TODO(Samuel): Draw effects
}

struct TileEntitySize {
	Vec2i size;
	Vec2i origin;
};

global TileEntitySize te_sizes[4] = {
	{{0, 0}, {0, 0}},
	{{1, 2}, {0, 1}},
	{{1, 1}, {0, 0}},
	{{1, 1}, {0, 0}},
};

function b32
check_tile_entity_colision(TileEntityType type, Vec2i pos)
{
	TileEntitySize size = te_sizes[type];
	for (int y = 0; y < size.size.y; y++) {
		i32 yi = y - size.origin.y + pos.y;	
		if (yi < 0 || yi >= WORLD_SIZE) return true;

		for (int x = 0; x < size.size.x; x++) {
			i32 xi = x - size.origin.x + pos.x;	
			if (xi < 0 || xi >= WORLD_SIZE) return true;

			if (gw->te_ids[yi][xi] != 0) {
				return true;
			}
		}
	}

	return false;
}

function TeId
insert_tile_entity(TileEntityType type, Vec2i pos, i32 rotation)
{
	TeId id = 0;

	if (gw->te_list.count < TILE_ENTITY_LIST_CAPACITY) {
		TileEntity entity = {};
		entity.type = type;	
		entity.pos = pos;	
		entity.rotation = rotation;

		// Check colision
		if (check_tile_entity_colision(type, pos)) {
			return id;
		}

		for (int i = 1; i < gw->te_list.count; i++) {
			if (gw->te_list.data[i].id == 0) {
				id = i;
				break;
			}		
		}

		if (!id) {
			id = gw->te_list.count++;
		}

		entity.id = id;

		TileEntitySize size = te_sizes[type];
		for (int y = 0; y < size.size.y; y++) {
			i32 yi = y - size.origin.y + pos.y;	
			for (int x = 0; x < size.size.x; x++) {
				i32 xi = x - size.origin.x + pos.x;	
				gw->te_ids[yi][xi] = id;
			}
		}

		switch (type) {
			case TE_Null: {} break;

			case TE_Pole: {
				entity.sprite.texture = Asset_TowerSheet;
				entity.sprite.frame_size = {16, 32};
				entity.sprite.origin = {0, 16};
				entity.sprite.speed = 1;
				entity.sprite.animation = {0, rotation * 4, 4};
				entity.buffer_capacity = 100;
			} break;

			case TE_Drill: {
				entity.sprite.texture    = Asset_AutoDrillSheet;
				entity.sprite.frame_size = {16, 32};
				entity.sprite.speed = 1;
				entity.sprite.animation  = {0, 0, 38};
				entity.buffer_capacity = 10;
			} break;

			case TE_Base: {
				entity.sprite.frame_size = {32, 32};
				entity.sprite.origin     = {16, 16};
				entity.sprite.speed = 1;
				entity.sprite.animation  = {0, 0, 1};
				entity.buffer_capacity = 10000;
				entity.energy_buffer = 10000;
			} break;
		}

		gw->te_list.data[id] = entity;
	}

	return id;
}

function void
delete_tile_entity(TeId id)
{
}

function TileEntity *
te_ptr(TeId id)
{
	auto te = &gw->te_list.data[0];

	if (gw->te_list.data[id].id == id) {
		te = &gw->te_list.data[id];
	}
	
	return te;	
}


function TeId
get_teid(Vec2i pos) {
	if (pos.x < 0 || pos.y >= WORLD_SIZE || pos.y < 0 || pos.x >= WORLD_SIZE) {
		return 0;
	}
	return gw->te_ids[pos.y][pos.x];
}