#ifndef ANIMATION_H
#define ANIMATION_H

struct Animation {
	i32 row;
	i32 start, count;
};

struct Sprite {
	AssetId texture;
	Vec2i frame_size;
	Vec2i origin;

	float timer;
	float speed;
	i32 frame;

	Animation animation;
};

function void set_animation(Sprite *sprite, Animation animation);
function void draw_sprite(Sprite *sprite, Vec2 pos);

#endif // ANIMATION_H