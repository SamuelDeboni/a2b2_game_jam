#include <raylib.h>
#include <stdio.h>

#include "base/base.h"
#include "assets.h"
#include "animation.h"
#include "world.h"
#include "player.h"

// Util macros
#define VEC2_CAST(T, v) (T){(v).x, (v).y}

// ======= Globals ========
global Arena g_arena;
global Arena f_arena;

global World *gw; // game World
global Camera2D camera;

global TileEntityType selected_entity = TE_Null;
global i32 selected_rotation = 0;


int main() {
    // Open Window
    InitWindow(1280, 720, "Game");
    SetWindowState(FLAG_WINDOW_RESIZABLE);
    SetTargetFPS(60);

    // Get memory
    g_arena = create_arena(malloc_base_memory(), MEGABYTE(128));
    f_arena = create_arena(malloc_base_memory(), MEGABYTE(128));

    // Load assets
    load_assets();

    // Allocate resources
    gw = PUSH_ARRAY(&g_arena, World, 1); 
    gw->te_list.count = 1;

    // Set defaults
    camera.offset = (Vector2){(f32)(GetScreenWidth()/2), (f32)(GetScreenHeight()/2)};
    camera.target = (Vector2){ 0.0f, 0.0f};
    camera.rotation = 0.0f;
    camera.zoom = 4.0f;


    {
        u32 bytes_read = 0;
        u8 *data = LoadFileData("assets/IslandSmall.smp", &bytes_read);
        data += 15;
        for (int i = 0; i < (WORLD_SIZE*WORLD_SIZE); i += 1) {
            gw->tilemap[i] = data[i];
        }
    }

    insert_tile_entity(TE_Base, {61, 64});

    Player player = {};
    player.position = {70 * 16, 64 * 16};

    while (!WindowShouldClose())
    {
    	f_arena.offset = 0;

        if (IsWindowResized()) {
            camera.offset = (Vector2){(f32)(GetScreenWidth()/2), (f32)(GetScreenHeight()/2)};
        }

        if (IsKeyPressed(KEY_ONE)) {
            selected_entity = TE_Pole;
        }

        if (IsKeyPressed(KEY_TWO)) {
            selected_entity = TE_Drill;
        }

        if (IsKeyPressed(KEY_ESCAPE)) {
            selected_entity = TE_Null;
        }

        if (IsKeyPressed(KEY_R)) {
            selected_rotation = (selected_rotation + 1) % 4;
        }


        update_world();
        update_player(&player);

        { // Place stuff

        }

        camera.target = VEC2_CAST(Vector2, player.position);

        BeginDrawing();
        BeginMode2D(camera);
        {
            ClearBackground(BLACK);
            draw_world();

            DrawTextureEx(get_texture(Asset_Potato),
                VEC2_CAST(Vector2, player.position - 8.0f), 0, 0.5, WHITE);

            Vector2 world_mouse_pos = GetScreenToWorld2D(GetMousePosition(), camera);
            Vec2i mouse_tile_pos = {(i32)world_mouse_pos.x, (i32)world_mouse_pos.y};
            mouse_tile_pos = (mouse_tile_pos / 16);

            if (selected_entity != TE_Null) {

                TileEntity te = {};

                if (selected_entity == TE_Pole) {
                    te.type = selected_entity;
                    te.sprite.texture    = Asset_TowerSheet;
                    te.sprite.frame_size = {16, 32};
                    te.sprite.origin     = {0, 16};
                    te.sprite.animation  = {0, selected_rotation * 4, 4};
                    te.pos = mouse_tile_pos;
                    te.rotation = selected_rotation;
                } else if (selected_entity == TE_Drill){
                    te.type              = selected_entity;
                    te.sprite.texture    = Asset_AutoDrillSheet;
                    te.sprite.frame_size = {16, 16};
                    te.sprite.animation  = {0, 0, 38};
                    te.pos               = mouse_tile_pos;
                }
                draw_tile_entity(&te);

                if (check_tile_entity_colision(selected_entity, mouse_tile_pos)) {
                    DrawRectangle(mouse_tile_pos.x * 16, mouse_tile_pos.y * 16,
                                  16, 16, {255, 100, 50, 100});
                } else {
                    DrawRectangle(mouse_tile_pos.x * 16, mouse_tile_pos.y * 16,
                                  16, 16, {100, 200, 255, 100});

                    if (IsMouseButtonPressed(0)) {
                        insert_tile_entity(selected_entity, mouse_tile_pos, selected_rotation);
                        selected_entity = TE_Null;
                    }
                }
            } else {
                auto selected_id = get_teid(mouse_tile_pos); 
                auto te = te_ptr(selected_id);

                if (IsKeyPressed(KEY_R)) {
                    te->rotation = (te->rotation + 1) % 4;
                    if (te->type == TE_Pole) {
                        te->sprite.animation  = {0, te->rotation * 4, 4};
                    }
                }

                DrawRectangle(mouse_tile_pos.x * 16, mouse_tile_pos.y * 16,
                          16, 16, {100, 200, 255, 166});
            }
        }
        EndMode2D();
        EndDrawing();
    }
    CloseWindow();       	
}


// source files include

// NOTE(samuel): Use to format string
#include <string.h>

#include "base/base.cpp"
#include "world.cpp"
#include "player.cpp"
#include "assets.cpp"
#include "animation.cpp"