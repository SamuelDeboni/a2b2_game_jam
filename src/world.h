#ifndef WORLD_H
#define WORLD_H

enum TileEntityType {
	TE_Null = 0,
	TE_Pole,
	TE_Drill,
	TE_Base,
};

typedef u16 TeId;

struct TileEntity {
	TeId id;
	TileEntityType type;
	Vec2i pos;
	i32 rotation;
	Sprite sprite;

	f32 energy_buffer;
	f32 buffer_capacity;
};

#define WORLD_SIZE 128

#define TILE_ENTITY_LIST_CAPACITY (128 * 128)
struct TileEntityList {
	TileEntity data[TILE_ENTITY_LIST_CAPACITY];
	u32 count;
};


struct World {
	TileEntityList te_list;

	u8  tilemap[WORLD_SIZE * WORLD_SIZE];
	u16 te_ids[WORLD_SIZE][WORLD_SIZE];
};

function void update_world();

function void draw_tile_entity(TileEntity *te);
function b32 check_tile_entity_colision(TileEntityType type, Vec2i pos);
function TeId get_pole_conection(Vec2i *pos, i32 rotation);
function void draw_world();

function u16 insert_tile_entity(TileEntityType type, Vec2i pos, i32 rotation = 0);
function void delete_tile_entity(TeId id);
function TileEntity *te_ptr(TeId id);
function TeId get_teid(Vec2i pos);

#endif // WORLD_H